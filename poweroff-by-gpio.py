#!/usr/bin/env python2.7
try:
    import RPi.GPIO as GPIO
except:
    print("have you install RPi.GPIO pythom module?")
    print("please visit: 'https://code.google.com/p/raspberry-gpio-python/'")
    exit(1)
PIN=23
def action():
    poweroff_cmd=['poweroff']
    try:
        from subprocess import Popen
        print ("now will poweroff ...")
        Popen(poweroff_cmd);
    except:
        print("subprocess except!")
        try:
            from os import system
            print ("now will poweroff ...")
            system(poweroff_cmd)
        except:
            print("system except!")
            print ("now will poweroff ...")
            from subprocess import call
            call(poweroff_cmd)


GPIO.setmode(GPIO.BCM)

GPIO.setup(PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

try:
    GPIO.wait_for_edge(PIN, GPIO.FALLING)
    GPIO.cleanup()
    action()
except KeyboardInterrupt:
    GPIO.cleanup()

