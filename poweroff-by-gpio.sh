#!/bin/bash
WAIT_S=60

GPIN=23 

[[ -d /sys/class/gpio/gpio${GPIN}/ ]] || \
	echo ${GPIN} > /sys/class/gpio/export 

echo in > /sys/class/gpio/gpio${GPIN}/direction 

# PRE START
for i in `seq 1 $WAIT_S` ; do
	echo "SEQ $i"
	if [ "`cat /sys/class/gpio/gpio${GPIN}/value`" -eq "1" ] ; then
		echo "GPIO ${GPIN} : `cat /sys/class/gpio/gpio${GPIN}/value`"
		break
	fi
	if [ "$i" -eq "$WAIT_S" ] ; then
		echo "poweroff by gpio disabled"
		exit 1
	fi
	sleep 1
done

# START!

while [ "`cat /sys/class/gpio/gpio${GPIN}/value`" -eq "1" ] ; do
	sleep 1
done
if [ "`cat /sys/class/gpio/gpio${GPIN}/value`" -eq "0" ] ; then
	echo "poweroff"
	systemctl poweroff 
fi
