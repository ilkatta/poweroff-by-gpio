# -*- coding: utf-8 -*-
'''
    Poweroff by GPIO Button
    Copyright (C) 2013 Katta

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import xbmc,os
from time import sleep

class Service():
    __enable = False
    pin = 23
    def __init__(self):
        if not os.path.isdir('/sys/class/gpio/gpio%s/' % self.pin ):
            with open('/sys/class/gpio/export','w') as exfd:
                exfd.write(str(self.pin))
            with open('/sys/class/gpio/gpio%s/direction' % self.pin , 'w') as dirfd:
                dirfd.write('in')
            with open('/sys/class/gpio/gpio%s/value' % self.pin , 'r') as pvalfd :
                pval = pvalfd.read()
            try:
                self.__enable = int(pval) is 1
            except ValueError:
                self.__enable = False

    def isEnabled(self):
        return self.__enable

    def waitEvent(self):
        wait=True
        while wait:
            if self.isPressed():
                wait = True
            else:
                wait = False
                return True
            sleep(5)
        return False

    def isPressed(self):
        with open('/sys/class/gpio/gpio%s/value' % self.pin , 'r') as gpiofd:
            try:
                pval = int( gpiofd.read() )
                return pval is 1
            except ValueError:
                return False

