# -*- coding: utf-8 -*-
'''
    Poweroff by GPIO Button
    Copyright (C) 2013 Katta

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import xbmc
import PowerGpio
import xbmcaddon

__scriptid__ = 'service.poweroff.gpio'
__addon__ = xbmcaddon.Addon(id=__scriptid__)
__cwd__ = __addon__.getAddonInfo('path')

xbmc.executebuiltin('Notification(%s, %s, 5000, %s/icon.png)' % (__scriptid__,"hello" , __cwd__) )

gpio = PowerGpio.Service()

if gpio.isEnabled() :
    interup=False
    while not xbmc.abortRequested or not interup:
        if not gpio.isPressed():
            interup=True
            xbmc.executebuiltin('ShutDown()')
        sleep(5)
else:
    xbmc.executebuiltin('Notification(%s, %s, 5000, %s/icon.png)' % (__scriptid__,"Button not connected" , __cwd__) )